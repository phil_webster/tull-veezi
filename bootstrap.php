<?php

use tull\veezi\Start;

/**
 * Register Autoloader
 */
spl_autoload_register(function ($class) {
	$prefix = 'tull\\veezi\\';
	$base_dir = __DIR__ . '/classes/';
	$len = strlen($prefix);
	if (strncmp($prefix, $class, $len) !== 0) {
		return;
	}
	$relative_class = substr($class, $len);
	$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
	if (file_exists($file)) {
		require $file;
	}

});

/**
 * Boot it
 */
add_action( 'init', 'tull_veezi_film_post_type', 0 );
add_action( 'init', 'tull_veezi_slider_post_type', 0 );
add_action( 'init', function(){
	new Start();
}, 1 );


include_once( TULL_VEEZI_PATH . '/includes/post-types.php' );
//include_once( TULL_VEEZI_PATH . '/includes/cron.php');


/* Hook into the REST API and include featured_image in "wp-json/wp/v2/tull_slider/" response 
*
*/
function slug_register_featured_img() {
   register_rest_field( 'tull_slider',
	   'featured_image', // Add it to the response
	   array(
		   'get_callback'    => 'get_featured_image_from_acf', // Callback function - returns the value
		   'update_callback' => null,
		   'schema'          => null,
	   )
   );
}
add_action( 'rest_api_init', 'slug_register_featured_img' );

/**
* Get the value of the "image" ACF field
*
* @param array $object Details of current post.
* @param string $field_name Name of field.
* @param WP_REST_Request $request Current request
*
* @return mixed
*/
function get_featured_image_from_acf( $object, $field_name, $request ) {
   // Check if ACF plugin activated
   if ( function_exists( 'get_field' ) ) {
	   // Get the value
		if ( $slides = get_field( 'slider_imgs', $object['id'] ) ) :
			$array_of_ids = array_column($slides, 'image');
			$array_of_images = array_map( function($id){
				$image_src = wp_get_attachment_image_src($id, 'full');
				return $image_src[0];
			}, $array_of_ids );
			
			
			return $array_of_images;
	   endif;
   } else {
	   return '';
   }
}
add_action( 'init', 'add_tull_slider_image_size' );
function add_tull_slider_image_size(){
	add_image_size( 'tullslider', 1900, 600, true );
	
}
