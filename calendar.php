
<script type="text/javascript" src="/wp-content/uploads/2016/10/clndr.js"></script>



<div id="full-clndr" class="clearfix">
	<div style="width:100%">
		<div id="calendar"></div>
	</div>
   <!-- Template containing the layout for the calendar-->
   <script type="text/template" id="full-clndr-template">
<div class="clndr-all">
     <div class="clndr-controls">
       <div class="clndr-previous-button">&lt;</div>
       <div class="clndr-next-button">&gt;</div>
       <div class="current-month"><%= month.toUpperCase() %> <%= year %></div>
   
     </div>
     <div class="clndr-grid">
       <div class="days-of-the-week clearfix">
         <% _.each(daysOfTheWeek, function(day) { %>
           <div class="header-day"><%= day %></div>
         <% }); %>
       </div>
       <div class="days">
         <% _.each(days, function(day) { %>
           <div class="<%= day.classes %> clndr-<%=day.date.format('YYYY-MM-DD')%>" id="<%= day.id %>"><span class="day-number"><%= day.day %></span></div>
         <% }); %>
       </div>
</div>
     </div>
     <div class="event-listing">
       <div class="event-listing-title">SHOWTIMES</div>
       <% if (extras.$eventsToday.length == 0) {%><div class="event-item"><%=extras.$noRecordsAvailableMessage%></div><%}%>
       <% _.each(fnCoalate(extras.$eventsToday), function(movie) { %>
        <div class="event-item">
           <div class="event-item-name"><%= movie.title %></div>
           <% _.each(movie.events, function(showing) { %>
            <div class="session-time"><a href="<%=showing.url%>" <%=showing.addId%> target="_blank"><%= showing.showTime %></a></div>
              <div class="session-message"><%=showing.message%></div>
           <% }); %>
        </div>
      <% }); %>
     </div>
   </script>
</div>

<script>
                   
   function fnCoalate(events) 
   {
      var coalatedEvents = [];
      var ocoalations = {};
      $.each
      (
         events, 
         function(index, event)
         {
            if(ocoalations[event.title] == null) { ocoalations[event.title] = [];}
            ocoalations[event.title].push(event)
         }
      );
      for(coalatedEvent in ocoalations)
      {
         coalatedEvents.push({ title : coalatedEvent, events : ocoalations[coalatedEvent]});
      }
      console.log(coalatedEvents);
      return coalatedEvents;
   }
   var $eventsToday = null;
   var $siteToken = '6avxdqr7sn7fr75807a5k307jm';                    ///This is the site Token
   var veeziAccessToken ='2yvp7tx3ckv22pn8hnxe44t5sw';               ///This is the Access Token for the Veezi web service
   var $clndr = $('#calendar').clndr
   (
      {
         extras      :  {
                           $eventsToday               : [], 
                           $noRecordsAvailableMessage : ' Showtimes and titles are being scheduled now. Please check back soon.' ///This is where you define the message that shows up when no showtimes are available for a selected date
                        },  
         template    : $('#full-clndr-template').html(),
         events      : [],
         clickEvents : {
                         click: function(data) 
                         {
                           
                            ///This function is called when a user clicks on a month to change the month
                            this.options.extras.$eventsToday = data.events;
                            this.render();

                            ///Now lets go ahead and find the day that was selected and highlight the cell
                            ///of that date with the selected-day class (which effectively is a simple css
                            ///class that can be adjusted based on the class of the site.
                            var className = '.clndr-' + data.date.format('YYYY-MM-DD')
                            var e = $(document).find(className);
                            e.addClass('selected-day');
                         },
            
                     },
      }
   );
   
   

   /**
      Called when the page is ready and fully loaded
   */
   function fnPage_Ready()
   {
      var $ajaxRequest = 
      {
         url      : 'https://api.us.veezi.com/v1/session',
         dataType : 'json',
         headers  : { 'VeeziAccessToken': veeziAccessToken }
      }
       $.get($ajaxRequest).done(fnMovieData_OnDownloaded);
   }

   /**
      This function processes downloaded data from teh Veezi REST service
   */
   function fnMovieData_OnDownloaded(response)
   {
      var soortedSessions = response.sort(function(a, b) { return a.PreShowStartTime.localeCompare(b.PreShowStartTime); });
      
      ///Lets build our list of events as returned from the veezi REST service
      var aCalendarEvents = [];
      $.each
      (
         soortedSessions, 
         function(index, session) 
         {
            if(session.Status == 'Open') {
   
               var attMessage = '';
               var alertMessage = '';
               if ( session.Attributes[0] === '0000000007' ) {
                  attMessage = "* <a href='https://www.thetullfamilytheater.org/sensory-friendly-screenings/'>Sensory Friendly Screening</a>";
                  alertMessage = 'id="low-sensory"';
               }
               if( session.TicketsSoldOut == true ){
                  attMessage = "SOLD OUT!";
                  alertMessage = 'id="sold-out"';
                  }
            var sessionDate = moment(session.PreShowStartTime);
            var dd = sessionDate.date();
            var mm = sessionDate.month()+1;
            if ((""+mm).length < 2) { mm = "0" + mm; }
            if ((""+dd).length < 2) { dd = "0" + dd; }
            aCalendarEvents.push({ showTime:sessionDate.format('h:mm A'), id : 'clndr_' + mm,   date:sessionDate.year() + '-'+mm+'-'+dd, title: session.Title, url: 'https://ticketing.us.veezi.com/purchase/' + session.Id + '?siteToken=' + $siteToken })
}
         }
      );

      var now = new Date();
      now.setHours(0,0,0,0);
      var startDate = moment(now);
      var endDate = startDate;
      $clndr.setEvents(aCalendarEvents);

      var initializationEvents = $($clndr.options.events).filter
      (
         /**
            Filter initialized events for today's date (startDate and 
            EndDate are set to today's date when the calendar is initialized
         */
          function () 
          {
              var afterEnd = this._clndrStartDateObject.isAfter(endDate),
                  beforeStart = this._clndrEndDateObject.isBefore(startDate);

              if (beforeStart || afterEnd) { return false; } 
              else { return true; }
          }
      ).toArray();
      console.log(initializationEvents);
      $clndr.options.extras.$eventsToday = initializationEvents;
      $clndr.render();
  
   }

   $(document).ready(fnPage_Ready)
        
</script>