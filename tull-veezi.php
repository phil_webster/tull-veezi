<?php
/*
Plugin Name: The Tull Family Theater Veezi Integration
Plugin URI:
Description: 
Version: 0.1.0
Author: Phil Webster
Author URI: http://forte-press.com
Text Domain: tull-veezi
Domain Path: /languages
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

use tull\veezi\Films;

// Define Contansts
define( 'TULL_VEEZI_PATH', dirname( __FILE__ ) );
define( 'TULL_VEEZI_URL', plugin_dir_url(__FILE__ ) );
define( 'TULL_VEEZI_VER', '0.1.0' );

/**
 * Hooks to setup plugin
 */
add_action( 'plugins_loaded', 'tull_veezi_bootstrap', 25 );

/**
 * Load plugin or throw notice
 *
 * @uses plugins_loaded
 */
function tull_veezi_bootstrap(){

	$php_check = version_compare( PHP_VERSION, '5.4.0', '>=' );

	if ( ! $php_check ) {
		function tull_veezi_notice() {
			global $pagenow;
			if( 'plugins.php' !== $pagenow ) {
				return;
			}
			?>
			<div class="notice notice-error">
				<p><?php _e( 'Tull Veezi requires PHP 5.4 or later. Please update your PHP.', 'humans-of-ph' ); ?></p>
			</div>
			<?php
		}
		add_action( 'admin_notices', 'tull_veezi_notice' );

	}else{
		//bootstrap plugin
		require_once( dirname( __FILE__ ) . '/bootstrap.php' );

	}

}

// register_activation_hook(__FILE__, 'tull_activation');

// function tull_activation() {
//     if (! wp_next_scheduled ( 'tull_get_films' )) {
// 	wp_schedule_event(time(), 'hourly', 'tull_get_films');
//     }
// }

// add_action('tull_get_films', 'tull_veezi_get_films');

// function tull_veezi_get_films() {
//     $films = new Films();
//     $films->add_films_to_cpt();
// }
add_action('my_hourly_event', 'do_this_hourly');

function my_activation() {
    if ( !wp_next_scheduled( 'my_hourly_event' ) ) {
        wp_schedule_event( time(), 'hourly', 'my_hourly_event');
    }
}
add_action('init', 'my_activation');

function do_this_hourly() {
    $films = new Films();
    $films->add_films_to_cpt();
}

/**
 * Load Template for CPT
 */
function rt_include_gym_dashboard_page_template( $template ) {
	if ( is_singular( 'tull_film' )  ) {
			return plugin_dir_path( __FILE__ ) . 'includes/templates/film.php';
	}
	return $template;
}
add_filter( 'template_include', 'rt_include_gym_dashboard_page_template', 99 );
