<?php
namespace tull\veezi;

class Meta {

    const FILM_ID = 'tull_veezi_film_id';
    const GENRE = 'tull_veezi_genre';   
    const DISTRIBUTOR = 'tull_veezi_distributor';   
    const SHORT_NAME = 'tull_veezi_short_name';    
    const RATING = 'tull_veezi_rating';        
    const CONTENT = 'tull_veezi_content';      
    const DURATION = 'tull_veezi_duration';      
    const FORMAT = 'tull_veezi_format';        
    const PEOPLE = 'tull_veezi_people';        
    const THUMBNAIL_URL = 'tull_veezi_thumbnail_url'; 
    const TRAILER_URL = 'tull_veezi_trailer_url';   
}