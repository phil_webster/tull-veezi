<?php
namespace tull\veezi;

use tull\veezi\Veezi_API;
use tull\veezi\Meta;
use WP_Query;

class Films {

    protected $veezi;
    public function __construct(){
        $this->veezi = new Veezi_API();
        
    }

    public function add_films_to_cpt(){
        $films = $this->veezi->get_films_array();
        $film_ids = [];
        foreach( $films as $film ) {
            $query_args = array(
                'post_type' => [ Start::FILM_POST_TYPE ],
                'meta_query' => [
                    [
                    'key'     => Meta::FILM_ID,
                    'value'   => $film['Id'],
                    'compare' => '='
                    ]
                ]
            );
            $query = new WP_Query( $query_args );
            if( 0 == $query->post_count  ) {
                $this->add_film( $film );
            }
        }
    }

    public function create_film_by_id( $film_id ){
        $films = $this->veezi->get_films_array();
        $film = NULL;
        foreach( $films as $film ) {
            if( $film['Id'] == $film_id){
                $film = $film;
                break;
            }
        }
       return $this->add_film( $film );
        

    }

    public function add_film( $film ){
        $post_arr = [
            'post_type' => Start::FILM_POST_TYPE,
            'post_title' => $film['Title'],
            'post_content' => $film['Synopsis'],
            'post_status'   => 'publish',
            'meta_input' => [
                Meta::FILM_ID       => $film['Id'],
                Meta::GENRE         => $film['Genre'],
                Meta::DISTRIBUTOR   => $film['Distributor'],
                Meta::SHORT_NAME    => $film['ShortName'],
                Meta::RATING        => $film['Rating'],
                Meta::CONTENT       => $film['Content'],
                Meta::DURATION      => $film['Duration'],
                Meta::FORMAT        => $film['Format'],
                Meta::PEOPLE        => $film['People'],
                Meta::THUMBNAIL_URL => $film['FilmPosterThumbnailUrl'],
                Meta::TRAILER_URL   => $film['FilmTrailerUrl'],
            ]
        ];
       return wp_insert_post( $post_arr );

    }
    
}