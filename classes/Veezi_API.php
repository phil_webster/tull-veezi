<?php

namespace tull\veezi;

use tull\veezi\Meta;
use tull\veezi\Start;
use tull\veezi\Films;

class Veezi_API {

    const DAYS_FOR_NOW_PLAYING = 7;

    const VEEZI_ACCESS_HEADER = [
        'timeout'     => 20,
        'headers' => [
            'VeeziAccessToken' => '2yvp7tx3ckv22pn8hnxe44t5sw'
        ],
    ];
    
     
    public function get_now_playing_films(){
        $films_array = $this->get_films_array();
        $active_films = [];
        if ( $films_array) {
            foreach( $films_array as $film){   
                if( $film[ 'Status' ]  == 'Active' && $film['NationalCode'] == 'Site Open') {
                    if( $this->is_currently_showing( $film['Id'] ) ){
                        $active_films[] = $film;
                    }   
                }
            }
        }
        
        return $active_films;
    }

    protected function is_currently_showing( $film_id ){
        $currently_showing_films = $this->get_session_array();
        foreach ( $currently_showing_films as $currently_showing_film ) {
            if( $currently_showing_film['FilmId'] == $film_id ){
                $start_time = substr($currently_showing_film['FeatureStartTime'], 0, 10);
                $film_time = date_create( $start_time );
                $current_time = date_create( date('Y-m-d', time()));
                $interval = date_diff( $film_time, $current_time );
                if ($interval->d < self::DAYS_FOR_NOW_PLAYING ) {
                    return true;
                }
            }
        }
        return false;
    }

    public function get_coming_soon_films(){
        $films_array = $this->get_films_array();
        $active_films = [];
        if ( $films_array ) {
            foreach( $films_array as $film){
                if( $film[ 'Status' ]  == 'Active' && $film['NationalCode'] == 'Coming Soon' ) {
                    $active_films[] = $film;
                }
            }
        }
        
        return $active_films;
    }

    protected function get_advance_sales_films(){
        $films_array = $this->get_films_array();
       
        $advance_films = [];
        foreach( $films_array as $film){
            if(   $film[ 'Status' ]  == 'Active'  && $film['NationalCode'] == 'Advance' || ( $film[ 'Status' ]  == 'Active' && $film['NationalCode'] == 'Site Open'  &&   ! $this->is_currently_showing( $film['Id'] ) ) ) {
                $advance_films[] = $film;
            }
        }
        return $advance_films;   
    }

    public function get_films_array(){
        $args = self::VEEZI_ACCESS_HEADER;
        $films = wp_remote_get( 'https://api.us.veezi.com/v1/film/', $args );
        
        
        $body = wp_remote_retrieve_body( $films );
       
        return json_decode( $body, true);
    }

    public function get_session_array(){
        $args = self::VEEZI_ACCESS_HEADER;
        $session = wp_remote_get( 'https://api.us.veezi.com/v1/session', $args );
        $body = wp_remote_retrieve_body( $session );
        return json_decode( $body, true );

    }

    public function coming_soon() {
        $coming_soon_films = $this->get_coming_soon_films();
        return $this->preview_box( $coming_soon_films, false );
    }

    public function now_playing(){
        $now_playing_films = $this->get_now_playing_films();
        return $this->preview_box( $now_playing_films );
    }

    public function advance_sales(){
        $advance_sales_films = $this->get_advance_sales_films();
        return $this->preview_box( $advance_sales_films );
    }

    protected function preview_box( $films, $showing = true ){
        $html = '';
        foreach( $films as $film ){
            $film_url = $this->get_film_url( $film['Id'] ) ?: $this->create_film( $film['Id'] );
            $youtube_id = $this->get_youtube_id_from_url( $film['FilmTrailerUrl'] );
            $youtube_thumbnail_src = 'https://img.youtube.com/vi/' . $youtube_id . '/mqdefault.jpg';
            $html .= '<div class="col-film-box">';
                $html .= '<div class="video-wrapper">';
                    $html .= '<a href="'.$film['FilmTrailerUrl'].'" data-lity><img src="' . $youtube_thumbnail_src . '"><span class="video-button"><i class="fa fa-play-circle"></i></span></a>';
                $html .= '</div>'; 
                $html .= '<div class="movietitle">';
                    $html .= '<a href="' . $film_url . '">'. $film['Title']. '</a><br>';
                    
                    if( true == $showing ){
                        $html .= '<div class="moviebtn"><a href="' . $film_url . '">Showtimes and Tickets ➔ </div></a><br>'; 
                    }
                $html .= '</div>'; 
            $html .= '</div>'; 
        }
        return $html;
    }

    protected function get_youtube_id_from_url( $url ){
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
        return $match[1];
    }

    protected function get_film_url( $film_id ) {
        $args = [
            'post_type' => [ Start::FILM_POST_TYPE ],
                'meta_query' => [
                    [
                    'key'     => Meta::FILM_ID,
                    'value'   => $film_id,
                    'compare' => '='
                    ]
                ]
        ];
        $films = get_posts( $args );
        $id = $films[0]->ID ;
        return ($id === NULL) ? false : get_permalink( $id );
    }

    protected function create_film( $film_id ){
        $films = new Films();
        $post_id = $films->create_film_by_id( $film_id );
        $film_url = get_permalink( $post_id );
        return $film_url;
    }

    public function veezi_calendar(){
        ob_start();
        ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore.js"></script>
<script type="text/javascript" src="/wp-content/uploads/2016/10/clndr.js"></script>



<div id="full-clndr" class="clearfix">
	<div style="width:100%">
		<div id="calendar"></div>
	</div>
   <!-- Template containing the layout for the calendar-->
   <script type="text/template" id="full-clndr-template">
        
        <div class="clndr-all">
            <div class="clndr-controls">
                <div class="clndr-previous-button">&lt;</div>
                <div class="clndr-next-button">&gt;</div>
                <div class="current-month"><%= month.toUpperCase() %> <%= year %></div>
            </div>
            <div class="clndr-grid">
                <div class="days-of-the-week clearfix">
                    <% _.each(daysOfTheWeek, function(day) { %>
                        <div class="header-day"><%= day %></div>
                    <% }); %>
                </div>
                <div class="days">
                    <% _.each(days, function(day) { %>
                        <div class="<%= day.classes %> clndr-<%=day.date.format('YYYY-MM-DD')%>" id="<%= day.id %>"><span class="day-number"><%= day.day %></span></div>
                    <% }); %>
                </div>
            </div>
        </div>
        <div class="event-listing">
            <div class="event-listing-title">SHOWTIMES</div>
            <% if (extras.$eventsToday.length == 0) {%><div class="event-item"><%=extras.$noRecordsAvailableMessage%></div><%}%>
            <% _.each(fnCoalate(extras.$eventsToday), function(movie) { %>
            <div class="event-item">
           <div class="event-item-name"><%= movie.title %></div>
           <% _.each(movie.events, function(showing) { %>
            <div class="session-time <%=showing.soldOutClass%>"><a href="<%=showing.url%>" <%=showing.addId%> target="_blank"><%= showing.showTime %></a><%=showing.soldOut%></div>
              <div class="session-message"><%=showing.message%></div>
           <% }); %>
            </div>
            <% }); %>
        </div>
   </script>
</div>

<script>
                   
   function fnCoalate(events) 
   {
      var coalatedEvents = [];
      var ocoalations = {};
      $.each
      (
         events, 
         function(index, event)
         {
            if(ocoalations[event.title] == null) { ocoalations[event.title] = [];}
            ocoalations[event.title].push(event)
         }
      );
      for(coalatedEvent in ocoalations)
      {
         coalatedEvents.push({ title : coalatedEvent, events : ocoalations[coalatedEvent]});
      }
      console.log(coalatedEvents);
      return coalatedEvents;
   }
   var $eventsToday = null;
   var $siteToken = '6avxdqr7sn7fr75807a5k307jm';                    ///This is the site Token
   var veeziAccessToken ='2yvp7tx3ckv22pn8hnxe44t5sw';               ///This is the Access Token for the Veezi web service
   var $clndr = $('#calendar').clndr
   (
      {
         extras      :  {
                           $eventsToday               : [], 
                           $noRecordsAvailableMessage : ' Showtimes and titles are being scheduled now. Please check back soon.' ///This is where you define the message that shows up when no showtimes are available for a selected date
                        },  
         template    : $('#full-clndr-template').html(),
         events      : [],
         clickEvents : {
                         click: function(data) 
                         {
                           
                            ///This function is called when a user clicks on a month to change the month
                            this.options.extras.$eventsToday = data.events;
                            this.render();

                            ///Now lets go ahead and find the day that was selected and highlight the cell
                            ///of that date with the selected-day class (which effectively is a simple css
                            ///class that can be adjusted based on the class of the site.
                            var className = '.clndr-' + data.date.format('YYYY-MM-DD')
                            var e = $(document).find(className);
                            e.addClass('selected-day');
                         },
            
                     },
      }
   );
   
   

   /**
      Called when the page is ready and fully loaded
   */
   function fnPage_Ready()
   {
      var $ajaxRequest = 
      {
         url      : 'https://api.us.veezi.com/v1/session',
         dataType : 'json',
         headers  : { 'VeeziAccessToken': veeziAccessToken }
      }
       $.get($ajaxRequest).done(fnMovieData_OnDownloaded);
   }

   /**
      This function processes downloaded data from teh Veezi REST service
   */
   function fnMovieData_OnDownloaded(response)
   {
      var soortedSessions = response.sort(function(a, b) { return a.PreShowStartTime.localeCompare(b.PreShowStartTime); });
      
      ///Lets build our list of events as returned from the veezi REST service
      var aCalendarEvents = [];
      $.each
      (
         soortedSessions, 
         function(index, session) 
         {
if(session.Status == 'Open') {
                var attMessage = '';
               var alertMessage = '';
               if ( session.Attributes[0] === '0000000007' ) {
                  attMessage = "* <a href='https://www.thetullfamilytheater.org/sensory-friendly-screenings/'>Sensory Friendly Screening</a>";
                  alertMessage = 'id="low-sensory"';
               }
               var soldOut;
               var soldOutClass = '';
               if( session.TicketsSoldOut == true ){
                  soldOut = '<span class="sold-out-text">SOLD OUT!</span>';
                  alertMessage = 'id="sold-out"';
                  soldOutClass = 'sold-out';
                  }
            var sessionDate = moment(session.PreShowStartTime);
            var dd = sessionDate.date();
            var mm = sessionDate.month()+1;
            if ((""+mm).length < 2) { mm = "0" + mm; }
            if ((""+dd).length < 2) { dd = "0" + dd; }
            aCalendarEvents.push({ 
                showTime:sessionDate.format('h:mm A'), 
                id : 'clndr_' + mm,   date:sessionDate.year() + '-'+mm+'-'+dd, 
                title: session.Title, 
                url: 'https://ticketing.us.veezi.com/purchase/' + session.Id + '?siteToken=' + $siteToken,
                message: attMessage, 
                addId: alertMessage,
                soldOut : soldOut,
                soldOutClass : soldOutClass,
                 })
}
         }
      );

      var now = new Date();
      now.setHours(0,0,0,0);
      var startDate = moment(now);
      var endDate = startDate;
      $clndr.setEvents(aCalendarEvents);

      var initializationEvents = $($clndr.options.events).filter
      (
         /**
            Filter initialized events for today's date (startDate and 
            EndDate are set to today's date when the calendar is initialized
         */
          function () 
          {
              var afterEnd = this._clndrStartDateObject.isAfter(endDate),
                  beforeStart = this._clndrEndDateObject.isBefore(startDate);

              if (beforeStart || afterEnd) { return false; } 
              else { return true; }
          }
      ).toArray();
      
      $clndr.options.extras.$eventsToday = initializationEvents;
      $clndr.render();
  
   }

   $(document).ready(fnPage_Ready)
        
</script>
<?php
return ob_get_clean();
//return $output;
    }
}