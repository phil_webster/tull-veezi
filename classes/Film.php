<?php
namespace tull\veezi;

use tull\veezi\Meta;

class Film {

    public $id;

    public $film_id;

    public $genre;

    public $distributor;

    public $short_name;

    public $rating;

    public $content;

    public $duration;

    public $format;

    public $people;

    public $thumbnail_url;

    public $trailer_url;
    
    public $producers;

    public $actors;


    public function __construct( $id ){
        $this->id = $id;
        $this->get_meta_fields();
        $this->get_producers();
        $this->get_actors();
    }

    protected function get_meta_fields(){
        $meta_fields = [
            'film_id'       => Meta::FILM_ID,
            'genre'         => Meta::GENRE,  
            'distributor'   => Meta::DISTRIBUTOR,  
            'short_name'    => Meta::SHORT_NAME,   
            'rating'        => Meta::RATING,       
            'content'       => Meta::CONTENT,       
            'duration'      => Meta::DURATION,    
            'format'        => Meta::FORMAT,      
            'people'        => Meta::PEOPLE,        
            'thumbnail_url' => Meta::THUMBNAIL_URL,  
            'trailer_url'   => Meta::TRAILER_URL, 
        ];
        
        foreach( $meta_fields as $key => $meta_field){
            $meta_key = $meta_field;
            $this->$key = get_post_meta( $this->id, $meta_key, true );
        }
    }

    public function get_producers(){
        $people = $this->people;
        $producers = [];
        foreach( $people as $person ) {
            if($person['Role'] == 'Director'){
                $name = $person['FirstName'] . ' ' . $person['LastName'];
                $producers[] = $name;
            }
        }
        $this->producers = $producers;
    }

    public function get_actors(){
        $people = $this->people;
        $actors = [];
        foreach( $people as $person ) {
           
            if($person['Role'] == 'Actor'){
                $name = $person['FirstName'] . ' ' . $person['LastName'];
                $actors[] = $name;
            }
            
        }
        $this->actors = $actors;
    }

    public function the_actors(){
        $html = '';
        $actors = $this->actors;
        foreach( $actors as $actor ){
            $html .= '<div class="people">' . $actor  . '</div>';
        }
        return $html;
    }
}