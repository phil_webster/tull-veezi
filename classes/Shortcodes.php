<?php

namespace tull\veezi;

use tull\veezi\Veezi_API;
use tull\veezi\Carousel;

class Shortcodes {

	public $veezi;

	public function __construct() {
		$this->veezi = new Veezi_API();
		$this->register_shortcodes();
	}

	protected function register_shortcodes() {
		add_shortcode( 'coming_soon', [ $this, 'coming_soon' ] );
		add_shortcode( 'now_playing', [ $this, 'now_playing' ] );
		add_shortcode( 'veezi_calendar', [ $this, 'veezi_calendar' ] );
		add_shortcode( 'advance_sales', [ $this, 'advance_sales'] );
		add_shortcode( 'tull_carousel', [ $this, 'tull_carousel' ] );
	}

	public function now_playing() {
		return $this->veezi->now_playing();
	}

	public function coming_soon() {
		 return $this->veezi->coming_soon();
		
	}

	public function veezi_calendar() {
		return $this->veezi->veezi_calendar();
	}

	public function advance_sales() {
		return $this->veezi->advance_sales();
	}

	public function tull_carousel( $atts ) {
		$id = $atts['id']; 
		$carousel = new Carousel( $id );
		return $carousel->get_carousel();
	}
}
