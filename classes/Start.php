<?php

namespace tull\veezi;

use tull\veezi\Shortcodes;
use tull\veezi\Films;

class Start {

	/**
	 * Defines the film post type's name
	 *
	 * @since 0.0.1
	 */
	const FILM_POST_TYPE = 'tull_film';

	/**
	 * Defines the slider post type's name
	 *
	 * @since 0.0.1
	 */
	const SLIDER_POST_TYPE = 'tull_slider';

	public function __construct() {
		if( is_admin() ){
			$this->start_admin();
		}
		add_action( 'wp_enqueue_scripts', [$this, 'add_scripts'] );
		$this->add_shortcodes();
		$this->add_films();
	}


	protected function start_admin(){
		
	}

	protected function add_films(){
		new Films();
		
	}

	
	public function add_scripts(){
		wp_enqueue_script( 'moment', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js', ['jquery'] );
		wp_enqueue_script( 'clndr', TULL_VEEZI_URL . 'includes/js/clndr.js', ['moment', 'underscore'] );
        wp_enqueue_script( 'lity', TULL_VEEZI_URL . 'includes/vendor/lity/lity.min.js', ['jquery'], true );
		wp_enqueue_style( 'lity', TULL_VEEZI_URL . 'includes/vendor/lity/lity.min.css' );
		wp_enqueue_style( 'font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
		wp_enqueue_style( 'owl-css', TULL_VEEZI_URL . 'includes/vendor/owl/owl.carousel.min.css' );
		wp_enqueue_style( 'owl-theme-css', TULL_VEEZI_URL . 'includes/vendor/owl/owl.theme.default.min.css' );
		wp_enqueue_script( 'owl', TULL_VEEZI_URL . 'includes/vendor/owl/owl.carousel.min.js', [], true );
		wp_enqueue_script( 'tull-custom', TULL_VEEZI_URL . 'includes/js/custom.js', ['owl'], true );
	}

	

	protected function add_shortcodes() {
		$shortcodes = new Shortcodes();
		return $shortcodes;
	}

}
