<?php
namespace tull\veezi;

class Carousel {

	public $id;

	public function __construct( $id = 0 ){
		$this->id = $id;

	}
	
	public function get_carousel(){
		$slides = get_field( 'slider_imgs', $this->id );
		//die(var_dump( $slides ) );
		$html = '<div class="owl-carousel owl-theme">';
		foreach( $slides as $slide ){
			$image = wp_get_attachment_image( $slide['image'], 'tullslider' );
			$html .= '<div class="item">';
				$html .= '<a href="' . $slide['link'] . '">';
					$html .= $image;
				$html .= '</a>';
			$html .= '</div>';

		}
		$html .= '</div>';
		return $html;
	}
	
}