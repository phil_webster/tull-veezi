<?php
use tull\veezi\Start;

/**
 * Add Film
 */
function tull_veezi_film_post_type() {
		$labels = array(
		    'name' => 'Films',
		    'singular_name' => 'Film',
		    'add_new' => 'Add New',
		    'all_items' => 'All Films',
		    'add_new_item' => 'Add New Film',
		    'edit_item' => 'Edit Film',
		    'new_item' => 'New Film',
		    'view_item' => 'View Film',
		    'search_items' => 'Search Posts',
		    'not_found' =>  'No Film found',
		    'not_found_in_trash' => 'No Film found in trash',
		    'parent_item_colon' => 'Parent Post:',
		    'menu_name' => 'Films'
		);
		$args = array(
			'labels' => $labels,
			'description' => "Films",
			'taxonomies' => array('category'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 20,
			'menu_icon' => null,
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array('title','editor','thumbnail', 'custom-fields'),
			'has_archive' => false,
			'rewrite' => array('slug' => 'films', 'with_front' => FALSE),
			'query_var' => true,
			'can_export' => true
		);

		register_post_type( Start::FILM_POST_TYPE, $args );
}

/**
 * Add Film
 */
function tull_veezi_slider_post_type() {
	$labels = array(
		'name' => 'Sliders',
		'singular_name' => 'Slider',
		'add_new' => 'Add New',
		'all_items' => 'All Sliders',
		'add_new_item' => 'Add New Slider',
		'edit_item' => 'Edit Slider',
		'new_item' => 'New Slider',
		'view_item' => 'View Slider',
		'search_items' => 'Search Posts',
		'not_found' =>  'No Slider found',
		'not_found_in_trash' => 'No Slider found in trash',
		'menu_name' => 'Sliders'
	);
	$args = array(
		'labels' => $labels,
		'description' => "Sliders",
		'taxonomies' => array('category'),
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'show_in_rest'          => true,
		'menu_position' => 20,
		'menu_icon' => null,
		'capability_type' => 'post',
		'hierarchical' => false,
		'supports' => array('title'),
		'has_archive' => false,
		'rewrite' => array('slug' => 'sliders', 'with_front' => FALSE),
		'query_var' => true,
		'can_export' => true
	);

	register_post_type( Start::SLIDER_POST_TYPE, $args );
}

