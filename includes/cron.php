<?php

use tull\veezi\Films;

//register_activation_hook(__FILE__, 'tull_activation');

function tull_activation() {
    if (! wp_next_scheduled ( 'tull_get_films' )) {
	wp_schedule_event(time(), 'hourly', 'tull_get_films');
    }
}

//add_action('tull_get_films', 'tull_veezi_get_films');

function tull_veezi_get_films() {
    $films = new Films();
    $films->add_films_to_cpt();
}