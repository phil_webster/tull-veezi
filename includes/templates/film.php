<?php
use tull\veezi\Film;
$id =  get_the_ID();
$film = new Film( $id );


get_header();

?>
<article id="post-56428" class="page-body style-color-xsdn-bg post-56428 page type-page status-publish hentry page_category-films">
	<div class="post-wrapper">
		<div class="post-body">
        <div class="post-content">
        <div data-parent="true" class="row-container" data-section="0">
 
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="row double-top-padding double-bottom-padding single-h-padding limit-width row-parent" data-imgready="true">
  <div class="row-inner">
    <div class="pos-top pos-center align_left column_parent col-lg-8 single-internal-gutter">
      <div class="uncol style-light">
          <div class="uncoltable">
            <div class="uncell no-block-padding">
              <div class="uncont">
                  <div class="row-internal row-container">
                      <div class="row col-half-gutter row-child">
                          <div class="row-inner" style="height: 591px; margin-bottom: -1px;">
                            <div class="pos-top pos-center align_left column_child col-lg-3 single-internal-gutter">
                              <div class="uncol style-light">
                                <div class="uncoltable">
                                  <div class="uncell no-block-padding">
                                    <div class="uncont">
                                      <div class="uncode_text_column">
                                        <img src="<?php echo $film->thumbnail_url;?>" >
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="pos-top pos-center align_left column_child col-lg-9 single-internal-gutter">
                              <div class="uncol style-light">
                                <div class="uncoltable">
                                  <div class="uncell no-block-padding">
                                    <div class="uncont">
                                      <div class="uncode_text_column">
                                        <h2><span class="font-106379"><?php the_title(); ?></span></h2>
                                      </div>
                                      <div class="uncode_text_column">
                                        <p class="film-meta"><span class="font-106379"><strong>Rating: <?php echo $film->rating; ?></strong></span></p>
                                        <p class="film-meta"><span class="font-106379"><strong>Genre: <?php echo $film->genre; ?></strong></span></p>
                                        <p class="film-meta"><span class="font-106379"><strong>Duration: <?php echo $film->duration; ?></strong></span></p>
                                      </div>
                                      <div class="uncode_text_column">
                                        <p><span class="font-106379"> 
                                            <?php the_content(); ?>
                                        </span></p>
                                      </div>
                                      <div class="uncode_text_column">
                                        <p><span class="font-106379"> </span></p>
                                            <?php echo $film->the_actors(); ?>
                                      </div>
                                      <div class="uncode-wrapper uncode-share">
                                        <div class="share-button share-buttons share-inline sharer-0" style="display: block;">
                                          <label class="social-export"><span></span></label>
                                            <div class="social load top center networks-9">
                                              <ul>
                                                <li class="social-facebook" data-network="facebook" tabindex="0"></li>
                                                <li class="social-twitter" data-network="twitter" tabindex="0"></li>
                                                <li class="social-gplus" data-network="google_plus" tabindex="0"></li>
                                                <li class="social-pinterest" data-network="pinterest" tabindex="0"></li>
                                                <li class="social-linkedin" data-network="linkedin" tabindex="0"></li>
                                                <li class="social-xing" data-network="xing" tabindex="0"></li>
                                                <li class="social-paper-plane" data-network="email" tabindex="0"></li>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row-internal row-container mobile-hidden">
                          <div class="row row-child">
                            <div class="row-inner" style="height: 413px; margin-bottom: -1px;">
                              <div class="pos-top pos-center align_left column_child col-lg-12 single-internal-gutter">
                                <div class="uncol style-light">
                                  <div class="uncoltable">
                                    <div class="uncell no-block-padding">
                                      <div class="uncont">
                                        <div class="uncode_text_column">
                                          <p><span class="font-106379">
                                          </span></p>
                                          <iframe width="720" height="405" src="<?php echo $film->trailer_url; ?>" frameborder="0" allowfullscreen=""></iframe>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <div class="pos-top pos-center align_left column_parent col-lg-4 single-internal-gutter">
              <div class="uncol style-light">
                <div class="uncoltable">
                  <div class="uncell no-block-padding">
                    <div class="uncont">
	                  <div class="wpb_raw_code wpb_raw_js">
		                <div class="wpb_wrapper">                                               

                        <script type="text/javascript">
                            
                        jQuery( document ).ready(function($){
                        
                        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                        
                        function getParameterByName(name, url) {
                            if (!url) {
                            url = window.location.href;
                            }
                            name = name.replace(/[\[\]]/g, "\\$&");
                            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                                results = regex.exec(url);
                            if (!results) return null;
                            if (!results[2]) return '';
                            return decodeURIComponent(results[2].replace(/\+/g, " "));
                        }
                        
                        $.ajax({
                            url: 'https://api.us.veezi.com/v1/session',
                            dataType: 'json',
                            headers: { 'VeeziAccessToken': '2yvp7tx3ckv22pn8hnxe44t5sw' }
                        }).done(function(data){
                            
                            // variables 
                            var filmId = '<?php echo $film->film_id; ?>';
                        //   var filmId = 'ST00000014';
                            var dateElem;
                            var available = false;
                            var currentDate = new Date();
                            
                            var sessions = {};
                            
                            //EST offset
                            utc = currentDate.getTime() + (currentDate.getTimezoneOffset() * 60000);
                            serverDate = new Date(utc + (3600000*-5.0));
                            estDate = serverDate.toLocaleString();
                            
                            // sort session
                            data = data.sort(function(a, b) {
                            return a.PreShowStartTime.localeCompare(b.PreShowStartTime);
                            });
                            
                            // loop through all the results
                            $.each(data, function(index, session){
                            
                            // sessions for current film 
                            // and status equal to 'Open'
                            if(session.FilmId === filmId && session.Status == 'Open') {

                                var sessionDate = moment(session.PreShowStartTime);
                                var dateId = 'date-' + sessionDate.year() + '-'+(sessionDate.month()+1)+'-'+sessionDate.date(); // date-YYYY-MM-DD
                                
                                if(!available) {
                                available = true;
                                $("#showtimes").html("");
                                }
                                
                                var time = sessionDate.format('h:mm A');
                               
                                dateElem = $('#'+dateId);
                                // create element if doesn't exist
                                if(!dateElem.length) {
                                  
                                dateElem = $('<div class="session-row" id="' + dateId + '"></div>');
                                dateElem.append('<div class="session-date">' + days[sessionDate.get('day')] + ' ' +(sessionDate.get('month')+1)+'/'+sessionDate.get('date') +'</div>');
                                $('#showtimes').append(dateElem);
                                sessions[dateId] = [];
                                }
                                
                                //sessions[dateId].push({id: session.Id, time: time});
                                dateElem.append('<div class="session-time"><a href="https://ticketing.us.veezi.com/purchase/' + session.Id + '?siteToken=6avxdqr7sn7fr75807a5k307jm" target="_blank">' + time  + '</a></div>');
                                if( session.Attributes[0] === '0000000007'){
                                  dateElem.append('<div style="display:block;">* <a style="text-decoration: underline; display:inline-block; padding-bottom: 20px;" href="https://www.thetullfamilytheater.org/low-sensory-screenings/">Low-Sensory Screening</a></div>');
                                  }
                                if( session.TicketsSoldOut == true ){
                                  dateElem.append('<div style="display:block; padding-bottom: 20px;"> Sold Out</div>');
                                }
                                
                            } // close if filmId and status Open
                            }); // close each data loop
                            
                            if(!available) {
                              $("#tickets-wrapper").addClass('no-showtimes');
                              $("#showtimes").html("No showtimes currently available");
                            }
                            
                        });
                        })

                        </script>
                        <div id="tickets-wrapper">
                          <h2>Tickets</h2>
                          <br>
                          <div id="showtimes">
                            Loading...
                          </div>
                        </div>
                        </div>
	                    </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
            </div>
            <script id="script-292698" type="text/javascript">UNCODE.initRow(document.getElementById("script-292698"));</script>
        </div>
    </div>
    <?php endwhile; endif; ?>
</div>
</div>
</div>
</div>
</article>
<style>

.film-meta {
  margin-top: 0;
  margin-bottom: 0;
}
#tickets-wrapper {
  margin-top: -25px;
}
.session-time {
  color: #000000;
  width: 20%;
  display: inline-block;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 5px;
  padding-right: 5px;
  border: solid 1px;
  border-radius: 2px;
  text-align: center;
  margin-right: 10px;
  margin-top: 10px;
}
.session-time:hover {
  color: white;
  background: #e0e0e0;
  border-radius: 2px;
  border: solid 1px;
  border-color: black;
}
.session-date {
    font-weight: bold;
    padding: 5px 0;
}
.no-showtimes {
  display: none;
}
    
</style>
<?php
get_footer();
